def bubble(list)

  sorted = list.clone

  for i in 0..(sorted.length - 1)
    for j in 0..(sorted.length - i - 2)
      if (sorted[j + 1] <=> sorted[j]) == -1
        sorted[j], sorted [j + 1] = sorted[j + 1], sorted[j]
      end
    end
  end

  sorted
end

puts open('text.txt').readlines.sort
puts "************************"
puts bubble(open('text.txt').readlines)