
# https://github.com/karan/Projects#numbers

# Numbers

# Find PI to the Nth Digit - Enter a number and have the program generate PI
# up to that many decimal places. Keep a limit to how far the program will go.

def getPI num
  (Math::PI).to_s[0..num+1].to_f
end

puts getPI 12
puts getPI 4
puts getPI 5

